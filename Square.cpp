/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"
using namespace std;
Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double A) {
	a = A;
	b = A;
}

void Square::setB(double B) {
	b = B;
	a = B;
}

double Square::calculateCircumference() {
	return (a + b) * 2;
}

double Square::calculateArea() {

	return a * b;
}